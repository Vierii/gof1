package demo.infra;

/**
 * Created by jan on 3/12/2016.
 *
 */
public interface Repository<K,V> {

	boolean update(V value);

	V insert(V value);

	long count();

	V getById(K key);

}
