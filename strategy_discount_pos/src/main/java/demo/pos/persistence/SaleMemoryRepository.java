package demo.pos.persistence;

import demo.pos.domain.sale.Sale;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class SaleMemoryRepository implements SaleRepository {

    // This is a repository for which ID's are generated
    private static AtomicLong nextId=new AtomicLong(1);
    private Map<Long,Sale> sales= new HashMap<>();


    SaleMemoryRepository() {
    }


    public boolean update(Sale sale) {
        return sales.put(sale.getId(),sale) != null;
    }
    public Sale insert(Sale sale) {
        long id = nextId.getAndIncrement();
        sale.setId(id);
        sales.put(id,sale);
        return sale;
    }
    @Override
    public long count() {
        return sales.size();
    }

    @Override
    public Sale getById(Long key) {
        return sales.get(key);
    }
}