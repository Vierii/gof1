package demo.pos.domain.product;

/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductDescription {
    private long productId;
    private String desc;
    private double price;

    public ProductDescription(int productId, String desc, double price) {
        this.productId = productId;
        this.desc = desc;
        this.price = price;
    }

    public ProductDescription(String desc, double price) {
        this(-1,desc,price);
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
