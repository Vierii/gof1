package domain;

import publisher.Publisher;
import publisher.Subscriber;

import java.util.ArrayList;
import java.util.List;


public class Punt implements Publisher {
    private final List<Subscriber> subscribers;
    private int x;
    private int y;


    public Punt(int x, int y) {
        this.x = x;
        this.y = y;
        this.subscribers = new ArrayList<>();
    }

    public void verdubbelX() {
        x *= 2;
        this.notifySubscribers("doubled y");
    }

      private void notifySubscribers(String action) {
        subscribers.forEach(s -> s.update(action, this));
      }

    public void verdubbelY() {
        y *= 2;
        this.notifySubscribers("doubled X");
    }

    @Override
    public String toString() {
        return "(x,y) = (" + x + "," + y + ")";
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    @Override
    public void unsubscribe(Subscriber subscriber) {
        subscribers.remove(subscriber);
    }
}
